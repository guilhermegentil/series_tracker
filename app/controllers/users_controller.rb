class UsersController < ApplicationController

  # Create a new user on database, respond to [POST]/users/new
  def create
    @user = User.new(user_params)
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
    else
      render 'new' # Default /user/new
    end
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
    if !signed_in? && !current_user.equal?(@user)
      redirect_to root_path
    end
  end

  # New mass-assignment protection for Rails 4
  private
    def user_params
      params.require(:user).permit(:name, :surname, :gender, :birthday, :email, :password, :password_confirmation)
    end

end