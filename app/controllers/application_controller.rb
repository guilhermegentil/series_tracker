class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include SessionsHelper

  protect_from_forgery with: :exception
  before_filter :set_config

  Tmdb::Api.key("2701947a8f4b238d3f1ef3b98f51922a") # TMDB key

  def set_config
    @tmdb_config = Tmdb::Configuration.new
  end

end
