class ProgramsController < ApplicationController

  def show
      @program = get_or_save_program(params[:tmdb_id])
  end

  def update
    program = Program.find(params[:id])

    new_favorite_list = Favorite.new(user_id:current_user.id, program_id:program.id)

    if new_favorite_list.save
      flash[:success] = "New Show added!"
    else
      flash[:warning] = "This Show is already in your favorites list!"
      redirect_to program
      return
    end
    redirect_to current_user
  end

  def destroy
    current_user.favorites.find_by_program_id(params[:id]).destroy
    flash[:success] = "The show was removed!"
    redirect_to current_user
  end

  def search
    tmdb_search_result_set = Tmdb::TV.find(params[:q])

    @programs_list = []
    tmdb_search_result_set.each { |tmdb_result| @programs_list.push(get_or_save_program(tmdb_result.id)) }

    render 'list'
  end

  private
    def get_or_save_program(tmdb_id)
      existing_program = Program.find_by_tmdb_id(tmdb_id)

      if existing_program.nil?

        data = Tmdb::TV.detail(tmdb_id)

        new_program = Program.new

        new_program.tmdb_id = data.id
        new_program.backdrop_path = data.backdrop_path
        new_program.original_name = data.original_name
        new_program.first_air_date = data.first_air_date
        new_program.poster_path = data.poster_path
        new_program.popularity = data.popularity
        new_program.name = data.name
        new_program.vote_average = data.vote_average
        new_program.vote_count = data.vote_count
        new_program.created_by = data.created_by
        new_program.episode_run_time = data.episode_run_time
        new_program.genres = data.genres
        new_program.homepage = data.homepage
        new_program.in_production = data.in_production
        new_program.languages = data.languages
        new_program.last_air_date = data.last_air_date
        new_program.number_of_episodes = data.number_of_episodes
        new_program.number_of_seasons = data.number_of_seasons
        new_program.origin_country = data.origin_country
        new_program.overview = data.overview
        new_program.seasons = data.seasons
        new_program.status = data.status

        blank_image = "no_image.png"

        new_program.small_image    = data.poster_path.nil? ? blank_image : @tmdb_config.base_url + "w154" + data.poster_path
        new_program.medium_image   = data.poster_path.nil? ? blank_image : @tmdb_config.base_url + "w342" + data.poster_path
        new_program.large_image    = data.poster_path.nil? ? blank_image : @tmdb_config.base_url + "w500" + data.poster_path
        new_program.original_image = data.poster_path.nil? ? blank_image : @tmdb_config.base_url + "original" + data.poster_path

        new_program.save
        new_program
      else
        existing_program
      end

    end

end
