class Season < ActiveRecord::Base
  belongs_to :program
  has_many :episodes
end
