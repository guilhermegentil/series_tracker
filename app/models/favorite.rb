class Favorite < ActiveRecord::Base
  belongs_to :user
  belongs_to :program


   # The touple (:user_id, :program_id) must be unique, so the same Show does not
   # appear more than one time
  validates_uniqueness_of :user_id, scope: :program_id

end
