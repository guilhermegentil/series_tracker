class User < ActiveRecord::Base


    has_many :favorites
    has_many :programs, through: :favorites


    # Allowed by gem bcrypt-ruby to encypt passwords.
    has_secure_password

    # Do some information treatment before save it on database.
    before_save do |user|
        user.email = email.downcase # To avoid conflicts on database store all emails as lower case.
        user.remember_token = SecureRandom.urlsafe_base64 # Generates a unique id for each user, used for session management.
    end

    # REGEX to validate emails strings with the format [STRING]@[STRING].[STRING]
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    # Field validations
    validates :name, presence: true, length: { minimum:1, maximum:50 }
    validates :surname, presence: true, length: { minimum:1, maximum:50 }
    validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
    validates :birthday, presence: true
    validates :gender, presence: true, inclusion: {in: ['MALE', 'FEMALE']}
    validates :password, presence: true, length: { minimum:6 }
    validates :password_confirmation, presence:true

end