class Episode < ActiveRecord::Base
  belongs_to :season
  has_many :users, through: :episode
end
