class Program < ActiveRecord::Base

  serialize :created_by
  serialize :episode_run_time
  serialize :genres
  serialize :origin_country
  serialize :seasons
  serialize :languages

  has_many :favorites
  has_many :users, through: :favorites

  def release_year
    first_air_date.nil? ? "unknow" : first_air_date.year
  end

  def creators
    str = ""
    created_by.each {|creator| str += creator['name'] + ", "}
    str
  end

  def countries
    str = ""
    origin_country.each {|country| str += country + ", "}
    str
  end

end
