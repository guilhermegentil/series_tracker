module ProgramsHelper
  def is_current_user_favorite?(program)
    current_user.programs.include?(program)
  end
end
