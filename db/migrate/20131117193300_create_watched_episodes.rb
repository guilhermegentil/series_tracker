class CreateWatchedEpisodes < ActiveRecord::Migration
  def change
    create_table :watched_episodes do |t|
      t.integer :user_id
      t.integer :espisode_id
      t.text :comment

      t.timestamps
    end
  end
end
