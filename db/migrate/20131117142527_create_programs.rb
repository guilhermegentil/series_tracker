class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.integer :tmdb_id
      t.string  :small_image
      t.string  :medium_image
      t.string  :large_image
      t.string  :original_image
      t.string  :backdrop_path
      t.string  :original_name
      t.date    :first_air_date
      t.string  :poster_path
      t.float   :popularity
      t.string  :name
      t.float   :vote_average
      t.integer :vote_count
      t.text    :created_by #ARRAY
      t.text    :episode_run_time #ARRAY
      t.text    :genres #ARRAY
      t.string  :homepage
      t.boolean :in_production
      t.text    :languages
      t.date    :last_air_date
      t.integer :number_of_episodes
      t.integer :number_of_seasons
      t.text    :origin_country #ARRAY
      t.text    :overview #Long field
      t.text    :seasons #ARRAY
      t.string  :status

      t.timestamps
    end

    add_index :programs, :tmdb_id, unique: true
  end
end
