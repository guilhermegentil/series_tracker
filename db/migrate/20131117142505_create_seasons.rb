class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.integer :season_number
      t.text :description
      t.float :audience_score
      t.integer :year
      t.integer :program_id

      t.timestamps
    end
  end
end
