class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.integer :episode_number
      t.string :title
      t.integer :duration
      t.date :airdate
      t.text :description
      t.float :audience_score
      t.float :imdb_score
      t.string :imdb_link
      t.integer :season_id

      t.timestamps
    end
  end
end
