# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131117193300) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "episodes", force: true do |t|
    t.integer  "episode_number"
    t.string   "title"
    t.integer  "duration"
    t.date     "airdate"
    t.text     "description"
    t.float    "audience_score"
    t.float    "imdb_score"
    t.string   "imdb_link"
    t.integer  "season_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "favorites", force: true do |t|
    t.integer  "user_id"
    t.integer  "program_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "programs", force: true do |t|
    t.integer  "tmdb_id"
    t.string   "small_image"
    t.string   "medium_image"
    t.string   "large_image"
    t.string   "original_image"
    t.string   "backdrop_path"
    t.string   "original_name"
    t.date     "first_air_date"
    t.string   "poster_path"
    t.float    "popularity"
    t.string   "name"
    t.float    "vote_average"
    t.integer  "vote_count"
    t.text     "created_by"
    t.text     "episode_run_time"
    t.text     "genres"
    t.string   "homepage"
    t.boolean  "in_production"
    t.text     "languages"
    t.date     "last_air_date"
    t.integer  "number_of_episodes"
    t.integer  "number_of_seasons"
    t.text     "origin_country"
    t.text     "overview"
    t.text     "seasons"
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "programs", ["tmdb_id"], name: "index_programs_on_tmdb_id", unique: true, using: :btree

  create_table "seasons", force: true do |t|
    t.integer  "season_number"
    t.text     "description"
    t.float    "audience_score"
    t.integer  "year"
    t.integer  "program_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "surname"
    t.date     "birthday"
    t.string   "email"
    t.string   "gender"
    t.string   "password_digest"
    t.string   "remember_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

  create_table "watched_episodes", force: true do |t|
    t.integer  "user_id"
    t.integer  "espisode_id"
    t.text     "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
