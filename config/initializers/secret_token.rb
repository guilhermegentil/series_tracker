# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
SeriesTracker::Application.config.secret_key_base = 'ff66282e490c09756047755484b5bdc4afe16ec2d55ebdb126ff4194a13f0add781e294f5c01077adbb024f6368e42301194adbe686bc110e30f02b802f01efa'
