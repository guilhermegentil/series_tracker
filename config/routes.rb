SeriesTracker::Application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  resources :users, :programs # Consider User as an REST resource with GET, POST, PUT, DELETE mapped as Rails actions
  resources :sessions, only: [:create, :destroy]

  root 'welcome#index'

  match '/signup' ,  to: 'users#new',        via: [:get, :post]
  match '/signin' ,  to: 'sessions#create',  via: 'post'
  match '/signout',  to: 'sessions#destroy', via: 'delete'

  match '/search',          to: 'programs#search', via: 'get'
  match '/program_details', to: 'programs#show',   via: 'get'

end
